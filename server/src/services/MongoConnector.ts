import * as mongoose   from 'mongoose';
import * as cachegoose from 'cachegoose';

import { Logger }    from '@core';
import { appConfig } from 'appConfig';

export class MongoConnector {
  static connect() {
    const log = new Logger('database');
    log.write(`Connecting to: ${appConfig.mongoUri}`);

    cachegoose(mongoose);

    mongoose
      .connect(appConfig.mongoUri, {
        useNewUrlParser: true
      })
      .then(() => log.write('Connected to MongoDB'))
      .catch(() => log.write('error', 'error'));
  }

}
