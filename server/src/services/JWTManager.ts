import { sign, verify } from 'jsonwebtoken';

import { appConfig }       from 'appConfig';
import { tokenRepository } from '@repository';
import { IToken }          from '@gql/interfaces/IToken';

export const encodeJWT = (payload: any = 'null'): string => {
  try {
    console.log(appConfig.jwtExpire);
    const token = sign(payload, appConfig.jwtSecret, {
      expiresIn: appConfig.jwtExpire || '24h'
    });
    tokenRepository.setToken(token);
    return token;
  } catch (e) {
    throw new Error(e);
  }
};

export const decodeJWT = async (token: string | undefined) => {

  const tokenInDB: IToken = await tokenRepository.getToken(token);

  if (tokenInDB && tokenInDB.isBlacklisted) {
    throw new Error('Token blacklisted');
  }
  return verify(token, appConfig.jwtSecret);

};