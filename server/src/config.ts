const DEBUG: string                      = process.env.DEBUG.substring(0, process.env.DEBUG.length - 1);
const mongoCredentials =  process.env.MONGO_USER && process.env.MONGO_PASSWORD
  ? `${process.env.MONGO_USER}:${encodeURIComponent(process.env.MONGO_PASSWORD)}@` : '';
const mongoUri                           = `mongodb://${mongoCredentials}\
${process.env.MONGO_URL}:${process.env.MONGO_PORT}/\
${process.env.MONGO_DB}`;

const configuration: config.Environments = {
  development: {
    clientIndexFile: 'index.html',
    folder         : {
      client: 'public/',
      server: 'src/'
    },
    endpoint       : {
      client: '/',
      api   : '/api'
    },
    defaultPort    : 3000,
    mongoUri       : mongoUri,
    debug          : DEBUG,
    jwtSecret      : process.env.JWT_SECRET,
    jwtExpire      : process.env.JWT_EXPIRE
  },
  test       : {
    clientIndexFile: 'index.html',
    folder         : {
      client: 'public/',
      server: 'src/'
    },
    endpoint       : {
      client: '/',
      api   : '/api'
    },
    defaultPort    : 3000,
    mongoUri       : mongoUri,
    debug          : DEBUG,
    jwtSecret      : process.env.JWT_SECRET,
    jwtExpire      : process.env.JWT_EXPIRE
  },
  production : {
    clientIndexFile: 'index.html',
    folder         : {
      client: 'public/',
      server: 'src/'
    },
    endpoint       : {
      client: '/',
      api   : '/api'
    },
    defaultPort    : 3000,
    mongoUri       : mongoUri,
    debug          : DEBUG,
    jwtSecret      : process.env.JWT_SECRET,
    jwtExpire      : process.env.JWT_EXPIRE
  }
};

export const appConfig: config.Configuration = (() => configuration[process.env.NODE_ENV])();
export const isProduction: boolean           = process.env.NODE_ENV === 'production';
