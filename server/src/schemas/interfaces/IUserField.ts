import { GraphQLFieldConfigMap, GraphQLFieldConfig } from 'graphql';

export interface IUserFields extends GraphQLFieldConfigMap<any, any> {
  id?: GraphQLFieldConfig<any, any>;
  name?: GraphQLFieldConfig<any, any>;
  email: GraphQLFieldConfig<any, any>;
  password: GraphQLFieldConfig<any, any>;
  roles?: GraphQLFieldConfig<any, any>;
  createdAt?: GraphQLFieldConfig<any, any>;
  updatedAt?: GraphQLFieldConfig<any, any>;
}
