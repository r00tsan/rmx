import { GraphQLFieldConfigMap, GraphQLFieldConfig } from 'graphql';

export interface ITokenFields extends  GraphQLFieldConfigMap<any, any> {
  token: GraphQLFieldConfig<any, any>;
  isBlacklisted?: GraphQLFieldConfig<any, any>;
}
