import { GraphQLResolveInfo } from 'graphql';

import { Context, Empty } from '@core';

export interface IAbstractField<R = {}, A = { [argName: string]: any }, S = {}> {
  allow: string[];
  before(context: Context<A>, args: A, source?: S): Promise<A>;
  after(result: R, context: Context<A>, args: A, source?: S): Promise<R>;
  execute(root: Empty, args: A, context: Context<A>, info: GraphQLResolveInfo): Promise<R>;
}