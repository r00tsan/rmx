export interface IQuestion {
  id: number;
  instructions?: string;
  stem: string;
  image?: string;
  answers: Array<string>;
  correctAnswer?: string;
}
