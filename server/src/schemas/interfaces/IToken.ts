export interface IToken {
  token: string;
  isBlacklisted?: boolean;
}
