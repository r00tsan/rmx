import { GraphQLFieldConfigMap, GraphQLFieldConfig } from 'graphql';

export interface ISuccessFields extends  GraphQLFieldConfigMap<any, any> {
  success: GraphQLFieldConfig<any, any>;
}
