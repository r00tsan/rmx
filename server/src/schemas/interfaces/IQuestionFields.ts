import { GraphQLFieldConfigMap, GraphQLFieldConfig } from 'graphql';

export interface IQuestionFields extends  GraphQLFieldConfigMap<any, any> {
  ids: GraphQLFieldConfig<any, any>;
  instructions?: GraphQLFieldConfig<any, any>;
  stem: GraphQLFieldConfig<any, any>;
  image?: GraphQLFieldConfig<any, any>;
  answers: GraphQLFieldConfig<any, any>;
  correctAnswer?: GraphQLFieldConfig<any, any>;
}
