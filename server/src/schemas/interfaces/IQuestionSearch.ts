export interface IQuestionSearch {
  query?: string;
  querySeparator?: string;
  limit: number;
}
