import { GraphQLObjectType } from 'graphql';

import { UserFields } from '@gql/fields';

export const UserType = new GraphQLObjectType({
  name       : 'UserType',
  description: 'A user',
  fields     : UserFields
});
