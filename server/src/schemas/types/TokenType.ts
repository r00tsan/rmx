import { GraphQLObjectType } from 'graphql';

import { TokenFields } from '@gql/fields';

export const TokenType = new GraphQLObjectType({
  name       : 'TokenType',
  description: 'A Json Web Token',
  fields     : TokenFields
});
