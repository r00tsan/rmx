import { GraphQLObjectType } from 'graphql';

import { QuestionFields } from '@gql/fields';

export const QuestionType = new GraphQLObjectType({
  name       : 'QuestionType',
  description: 'A question',
  fields     : QuestionFields
});
