import { GraphQLObjectType } from 'graphql';

import { SuccessFields } from '@gql/fields';

export const SuccessType = new GraphQLObjectType({
  name       : 'SuccessType',
  description: 'Success type shows if action done correct',
  fields     : SuccessFields
});
