import { GraphQLObjectType, GraphQLSchema } from 'graphql';

import {
  SignInMutation,
  SignOutMutation,
  UserCreateMutation
}                               from '@gql/mutations';
import { UserSearchQuery }      from '@gql/queries';
import { QuestionsFetchQuery } from '@gql/queries/QuestionsFetchQuery';

export class Schema {
  private static instance: Schema;

  private rootQuery: GraphQLObjectType = new GraphQLObjectType({
    name  : 'Query',
    fields: {
      userSearch: new UserSearchQuery(),
      getQuestions: new QuestionsFetchQuery()
    }
  });

  private rootMutation: GraphQLObjectType = new GraphQLObjectType({
    name  : 'Mutation',
    fields: {
      createUser: new UserCreateMutation(),
      login     : new SignInMutation(),
      logout    : new SignOutMutation()
    }
  });

  private schema: GraphQLSchema = new GraphQLSchema({
    query   : this.rootQuery,
    mutation: this.rootMutation
  });

  static get(): GraphQLSchema {
    if (!Schema.instance) {
      Schema.instance = new Schema();
    }
    return Schema.instance.schema;
  }
}
