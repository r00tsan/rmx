import {
  GraphQLFieldConfigArgumentMap,
  GraphQLObjectType,
} from 'graphql';

import { Empty, Logger }  from '@core';
import { IUserFields }    from '@gql/interfaces';
import { UserType }       from '@gql/types';
import { userRepository } from '@repository';
import { AbstractField }  from '@gql/fields';
import { EmailField }     from '@gql/fields/components';

const log: Logger = new Logger('schemas:queries:UserSearchQuery');

export class UserSearchQuery extends AbstractField {
  public allow: string[] = ['admin'];
  public type: GraphQLObjectType             = UserType;
  public description: string                 = 'Search for the user in database';
  public args: GraphQLFieldConfigArgumentMap = {
    email: new EmailField()
  };

  public execute = (_: Empty, args: IUserFields) => {
    log.write('Trying to find user by email');

    return userRepository.getByField('email', args.email);
  };
}