import {
  GraphQLFieldConfigArgumentMap,
  GraphQLList,
  GraphQLType
} from 'graphql';

import { Empty, Logger }           from '@core';
import { QuestionType }            from '@gql/types';
import { questionsRepository }     from '@repository';
import { AbstractField }           from '@gql/fields';
import { LimitField, StringField } from '@gql/fields/components';
import { IQuestionSearch }         from '@gql/interfaces';

const log: Logger = new Logger('schemas:queries:QuestionsFetchQuery');

export class QuestionsFetchQuery extends AbstractField {
  public allow: string[]                     = ['admin'];
  public type: GraphQLList<GraphQLType>      = new GraphQLList(QuestionType);
  public description: string                 = 'Receiving all questions from database';
  public args: GraphQLFieldConfigArgumentMap = {
    query: new StringField(),
    querySeparator: new StringField(),
    limit: new LimitField()
  };

  public execute = (_: Empty, args: IQuestionSearch) => {
    log.write('Trying to get all questions from DB');

    return questionsRepository.getQuestions(args);
  };
}