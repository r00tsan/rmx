import { GraphQLField, GraphQLString } from 'graphql';

export class NameField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'name';
  public description = 'The name of the object';
  public args;

}