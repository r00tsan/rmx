import { GraphQLField, GraphQLString } from 'graphql';

export class ImageField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'image';
  public description = 'The image of the question';
  public args;

}