import { GraphQLField } from 'graphql';

import { DateType } from '../../types';

export class CreatedAtField implements GraphQLField<undefined, undefined> {

  public type        = DateType;
  public name        = 'created at';
  public description = 'This is the date when the object was created';
  public args;
  public resolve(user) {

  }

}
