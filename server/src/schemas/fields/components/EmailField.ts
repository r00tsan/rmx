import { GraphQLField, GraphQLNonNull, GraphQLString } from 'graphql';

export class EmailField implements GraphQLField<undefined, undefined> {

  public type        = new GraphQLNonNull(GraphQLString);
  public name        = 'email';
  public description = 'The email address';
  public args;

}