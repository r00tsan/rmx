import { GraphQLField } from 'graphql';

import { DateType } from '@gql/types';

export class UpdatedAtField implements GraphQLField<undefined, undefined> {

  public type        = DateType;
  public name        = 'updated at';
  public description = 'This is the date when the object was updated';
  public args;

}
