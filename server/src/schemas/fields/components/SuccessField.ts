import { GraphQLField, GraphQLBoolean } from 'graphql';

export class SuccessField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLBoolean;
  public name        = 'success';
  public description = 'Show if action was successful or not';
  public args;

}