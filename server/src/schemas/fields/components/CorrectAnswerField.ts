import { GraphQLField, GraphQLString } from 'graphql';

export class CorrectAnswerField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'correctAnswer';
  public description = 'The correct answer on the question';
  public args;

}