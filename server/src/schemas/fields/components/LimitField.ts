import { GraphQLField, GraphQLInt } from 'graphql';

export class LimitField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLInt;
  public name        = 'limit';
  public description = 'The limit of loading questions';
  public args;

}
