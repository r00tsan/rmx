import { GraphQLField, GraphQLList, GraphQLString } from 'graphql';

export class RolesField implements GraphQLField<undefined, undefined> {

  public type        =  new GraphQLList(GraphQLString);
  public name        = 'is admin';
  public description = 'Whether or not the user is admin';
  public args;

}