import { GraphQLField, GraphQLString } from 'graphql';

export class TokenField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'password';
  public description = 'Encrypted password';
  public args;

}