import { GraphQLField, GraphQLString } from 'graphql';

export class StemField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'stem';
  public description = 'The stem of the question';
  public args;

}