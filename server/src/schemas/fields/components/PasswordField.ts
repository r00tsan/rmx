import { GraphQLField, GraphQLNonNull, GraphQLString } from 'graphql';

export class PasswordField implements GraphQLField<undefined, undefined> {

  public type        = new GraphQLNonNull(GraphQLString);
  public name        = 'password';
  public description = 'Encrypted password';
  public args;

}