import { GraphQLField, GraphQLID } from 'graphql';

export class IdField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLID;
  public name        = 'id';
  public description = 'The ID';
  public args;

}
