import { GraphQLField, GraphQLString } from 'graphql';

export class StringField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'string';
  public description = 'Some string field';
  public args;

}