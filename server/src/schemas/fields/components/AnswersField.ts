import { GraphQLField, GraphQLScalarType } from 'graphql';

export class AnswersField implements GraphQLField<undefined, undefined> {

  public type        =  new GraphQLScalarType({
    name: 'answers',
    serialize(value) {
      return value;
    },
  });
  public name        = 'answers';
  public description = 'Possible answers on question';
  public args;

}