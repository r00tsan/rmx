import { GraphQLField, GraphQLString } from 'graphql';

export class InstructionsField implements GraphQLField<undefined, undefined> {

  public type        = GraphQLString;
  public name        = 'instructions';
  public description = 'The instructions of the question';
  public args;

}