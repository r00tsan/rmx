import { GraphQLField, GraphQLInt, GraphQLList } from 'graphql';

export class IdsField implements GraphQLField<undefined, undefined> {

  public type        =  new GraphQLList(GraphQLInt);
  public name        = 'list of ids';
  public description = 'List of ids to related question';
  public args;

}