import {
  AnswersField,
  CorrectAnswerField,
  IdsField,
  ImageField,
  InstructionsField,
  StemField
}                          from '@gql/fields/components';
import { IQuestionFields } from '@gql/interfaces';

export const QuestionFields: IQuestionFields = {
  ids          : new IdsField(),
  instructions : new InstructionsField(),
  stem         : new StemField(),
  image        : new ImageField(),
  answers      : new AnswersField(),
  correctAnswer: new CorrectAnswerField()
};