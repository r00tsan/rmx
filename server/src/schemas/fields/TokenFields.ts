import { TokenField }   from '@gql/fields/components';
import { ITokenFields } from '@gql/interfaces';

export const TokenFields: ITokenFields = {
  token: new TokenField()
};