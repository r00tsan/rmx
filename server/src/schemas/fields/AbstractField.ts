import {
  GraphQLFieldConfig,
  GraphQLFieldConfigArgumentMap,
  GraphQLFieldResolver,
  GraphQLList,
  GraphQLOutputType,
  GraphQLResolveInfo,
  GraphQLType
} from 'graphql';

import { Context, Empty } from '@core';
import { IAbstractField }         from '@gql/interfaces/IAbstractField';

export class AbstractField<TResult = Empty, TSource = Empty, TArgs = { [argName: string]: any }>
  implements GraphQLFieldConfig<Empty, Context<TArgs>, TArgs>, IAbstractField {

  public allow: string[] = [];
  public type: GraphQLOutputType | GraphQLList<GraphQLType>;
  public description: string;
  public args: GraphQLFieldConfigArgumentMap;
  public resolve: GraphQLFieldResolver<Empty, Context<TArgs>> = async (root: Empty,
                                                                       args: TArgs,
                                                                       context: Context<TArgs>,
                                                                       info: GraphQLResolveInfo
  ): Promise<TResult> => {
    context.setResolveArgument(args);

    const validation = context.validateUser(this.allow);

    if (!validation.success) {
      context.Response.status(validation.status);
      return Promise.reject(validation.message);
    }

    args = await this.before(context, args);

    const result = await this.execute(root, args, context, info);

    await this.after(result, context, args);

    return result;
  };


  public before(context: Context<TArgs>, args: TArgs, source?: TSource): Promise<TArgs> {
    return Promise.resolve(args);
  }

  public after(result: TResult, context: Context<TArgs>, args?: TArgs, source?: TSource):
    Promise<TResult> {
    return Promise.resolve(result);
  }

  public execute(root: Empty, args: TArgs, context: Context<TArgs>, info: GraphQLResolveInfo):
    Promise<TResult> {
    return undefined;
  }

}