import {
  IdField,
  NameField,
  EmailField,
  PasswordField,
  RolesField,
  CreatedAtField,
  UpdatedAtField
}                      from './components';
import { IUserFields } from '../interfaces';

export const UserFields: IUserFields = {
  id       : new IdField(),
  name     : new NameField(),
  email    : new EmailField(),
  password : new PasswordField(),
  roles    : new RolesField(),
  createdAt: new CreatedAtField(),
  updatedAt: new UpdatedAtField()
};