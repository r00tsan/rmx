import { SuccessField }   from '@gql/fields/components';
import { ISuccessFields } from '@gql/interfaces/ISuccessFields';

export const SuccessFields: ISuccessFields = {
  success: new SuccessField()
};