import {
  GraphQLFieldConfigArgumentMap,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString
} from 'graphql';

import { Logger }         from '@core';
import { TokenType }      from '@gql/types';
import { IUserFields }    from '@gql/interfaces';
import { userRepository } from '@repository';
import { Empty }          from '@core';
import { AbstractField }  from '@gql/fields';

const log: Logger = new Logger('schemas:mutations:SignInMutation');

export class SignInMutation extends AbstractField {
  public type: GraphQLObjectType                           = TokenType;
  public description: string                               = 'Authenticate User';
  public args: GraphQLFieldConfigArgumentMap               = {
    email   : {type: new GraphQLNonNull(GraphQLString)},
    password: {type: new GraphQLNonNull(GraphQLString)}
  };

  public execute = (_: Empty, args: IUserFields) => {
    log.write('Trying authorize user');
    return userRepository.authorize(args);
  };
}