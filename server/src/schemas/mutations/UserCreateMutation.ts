import {
  GraphQLFieldConfigArgumentMap,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import { Logger }         from '@core';
import { UserType }       from '@gql/types';
import { IUserFields }    from '@gql/interfaces';
import { userRepository } from '@repository';
import { Empty }          from '@core';
import { AbstractField }  from '@gql/fields/AbstractField';

const log: Logger = new Logger('schemas:mutations:UserCreateMutation');

export class UserCreateMutation extends AbstractField {
  public allow = ['admin'];
  public type: GraphQLObjectType                           = UserType;
  public description: string                               = 'Create a user on database';
  public args: GraphQLFieldConfigArgumentMap               = {
    name    : {type: new GraphQLNonNull(GraphQLString)},
    email   : {type: new GraphQLNonNull(GraphQLString)},
    password: {type: new GraphQLNonNull(GraphQLString)}
  };

  public execute = (_: Empty, args: IUserFields) => {
    log.write('Trying to create new user');
    return userRepository.create(args);
  };

}