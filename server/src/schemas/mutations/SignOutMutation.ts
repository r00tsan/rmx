import {
  GraphQLObjectType
} from 'graphql';

import { Context, Logger } from '@core';
import { SuccessType }     from '@gql/types';
import { Empty }           from '@core';
import { AbstractField }   from '@gql/fields';
import { tokenRepository } from '@repository';
import { IToken }          from '@gql/interfaces/IToken';
import { ITokenFields }    from '@gql/interfaces';

const log: Logger = new Logger('schemas:mutations:SignInMutation');

export class SignOutMutation extends AbstractField {
  public allow: string[] = ['user', 'admin'];
  public type: GraphQLObjectType = SuccessType;
  public description: string     = 'Sign Out User';

  public before(context: Context<ITokenFields>, args: IToken): Promise<IToken> {
    args.token = context.Request['token'];
    return Promise.resolve(args);
  }

  public execute = (_: Empty, args: IToken) => {
    log.write('Trying to log out user');
    return tokenRepository.blacklistToken(args.token);
  };
}