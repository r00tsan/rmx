import { decodeJWT } from '@services/JWTManager';

export const AuthMiddleware = async (req, res, next) => {
  try {
    req.user = await decodeJWT(req.headers['x-access-token']);
    req.token = req.headers['x-access-token'];
  } catch (e) {
    req.user = {
      error: e.message
    };
  }

  next();
};