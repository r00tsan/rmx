import {
  Logger as LoggerInstance,
  createLogger,
  LoggerOptions,
  transports,
  format
}                 from 'winston';
import * as Debug from 'debug';

import { appConfig, isProduction } from 'appConfig';

const loggerOptions: LoggerOptions = {
  transports : [
    new transports.Console({
      level           : process.env.LOG_LEVEL,
      handleExceptions: isProduction,
      format          : format.combine(
        format.timestamp(),
        format.simple(),
        format.printf(info => `${info.timestamp} - ${info.level}: ${info.message}`),
        format.colorize({all: true})
      )
    })
  ],
  exitOnError: false
};

const logger: LoggerInstance = createLogger(loggerOptions);

const
  stream = (streamFunction) => ({
    'stream': streamFunction
  }),
  write  = (writeFunction) => ({
    write: (message: string) => writeFunction(message)
  });

const debug: Debug = Debug(`${appConfig.debug}response`);

export const winstonStream = stream(write(logger.info));
export const debugStream   = stream(write(debug));

export class Logger {
  private readonly levels: string[] = ['debug', 'verbose', 'silly', 'info', 'warn', 'error'];
  private readonly scope: string;
  private readonly scopeDebug: Debug;

  constructor(scope: string) {
    this.scope      = scope;
    this.scopeDebug = Debug(appConfig.debug + scope);
  }

  public write(message: string, level: string =  'debug', ...args: any[]): void {
    level = this.levels.indexOf(level) > -1 ? level : 'debug';

    if (level === 'debug') {
      if (isProduction) {
        logger[level](Logger.format(this.scope, message), Logger.parse(args));
      }
      this.scopeDebug(message, Logger.parse(args));
    } else {
      logger[level](Logger.format(this.scope, message), Logger.parse(args));
    }
  }

  static parse(args: any[]): any[] | string {
     return (args.length > 0) ? args : '';
  }

  static format (scope: string, message: string): string {
    return `[${scope}] ${message}`;
  }
}
