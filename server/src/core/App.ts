import * as express    from 'express';
import * as path       from 'path';
import * as morgan     from 'morgan';
import * as bodyParser from 'body-parser';

import { winstonStream, debugStream } from '@core';
import { appConfig }                  from 'appConfig';
import { IndexRoutes }                from '../routes';
import { MongoConnector }             from '@services/MongoConnector';
import { AuthMiddleware }             from './middleware/AuthMiddleware';

export class App {
  public app: express.Application = express();
  private readonly port: number;

  constructor(port) {
    this.port = port;
    this.setCommon();
    this.setRoutes();
  }

  static onListen(): void {
    MongoConnector.connect();
  }

  private setCommon() {
    this.app.set('port', this.port);
    this.app.set('jwt-secret', process.env.JWT_SECRET);

    this.app.use(morgan('dev', debugStream));
    this.app.use(morgan('combined', winstonStream));
    this.app.use(bodyParser.json());
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: false}));
    this.app.use(AuthMiddleware);
  }

  private setRoutes() {
    const indexRoutes: IndexRoutes = new IndexRoutes();

    this.app.use(express.static(path.join(__dirname, 'public')));
    this.app.use(appConfig.endpoint.client, indexRoutes.routes);

    indexRoutes.setErrorRoutes(this.app);
  }

}
