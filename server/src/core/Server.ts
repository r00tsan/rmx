import * as http from 'http';

import { appConfig } from 'appConfig';
import { Logger }       from '@core';
import { App }       from './App';

export class Server {
  static readonly port: string | number = Server.normalizePort(process.env.PORT);
  static readonly application: App      = new App(Server.port);
  static readonly running: http.Server  = http.createServer(
    Server.application.app
  );
  static readonly log: Logger = new Logger('server');

  static start() {
    Server.running.on('error', this.onError);
    Server.running.on('listening', this.onListening);
    Server.running.listen(Server.port);
  }

  static onListening() {
    const addr = Server.running.address();
    const bind = typeof addr === 'string'
      ? `pipe ${addr}`
      : `port ${addr.port}`;
    Server.log.write(`Listening on ${bind}`);
    App.onListen();
  }

  static onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind = typeof this.port === 'string'
      ? `Pipe ${Server.port}`
      : `Port ${Server.port}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        Server.log.write(`${bind} requires elevated privileges`, 'error');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        Server.log.write(`${bind} is already in use`, 'error');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  static normalizePort(port: string | number = appConfig.defaultPort) {
    return isNaN(Number(port)) ? 0 : port;
  }
}

