import { Response, Request } from 'express';

import { IUser } from '@gql/interfaces';

export class Context<A> {
  private args: A;

  constructor(
    private request: Request,
    private response: Response
  ) {
  }

  public get Args(): A {
    return this.args;
  }

  public get Response(): Response {
    return this.response;
  }

  public get Request(): Request {
    return this.request;
  }

  public validateUser(allowedRoles: string[]) {
    const userAllowed = this.hasUserRoles(allowedRoles);

    if (!userAllowed) {
      const errorMessage = this.request['user'].error
        ? this.request['user'].error
        : 'User not allowed';

      return {
        success: false,
        message: `Authorization:${errorMessage}`,
        status : 401
      };
    }

    return {
      success: true
    };
  }

  public hasUserRoles(allowedRoles: string[]): boolean {
    if (allowedRoles.length === 0) {
      return true;
    }

    const user: IUser | undefined = this.request['user'];

    if (!user || !user.roles) {
      return false;
    }

    for (let i = 0; i < allowedRoles.length; i++) {
      if (user.roles.indexOf(allowedRoles[i]) > -1) {
        return true;
      }
    }

    return false;
  }

  public setResolveArgument(args: A): void {
    this.args = args;
  }
}