export class Exception extends Error {

  static Separator = ':';
  static Name = 'UnknownException';

  static hasName(error: any | string | Error): boolean {
    let message = error;
    if (error.message) {
      message = error.message;
    }
    const reg = new RegExp('^[a-zA-Z]+:');
    return reg.test(message);
  }

  static getName(message: string): string {
    if (Exception.hasName(message)) {
      return message.split(Exception.Separator)[0];
    }
    return Exception.Name;
  }

  static getMessage(message: string): string {
    if (Exception.hasName(message)) {
      return message.split(Exception.Separator)[1];
    }

    return message;
  }

  constructor(...args: any[]) {
    super(args[0]);
    this.name = Exception.Name;
    this.message = args[0];
    this[Symbol()] = true;
    Error.captureStackTrace(this);
  }

  public toString(): string {
    return `${this.constructor.name}:${this.message}`;
  }
}
