import { Schema, Model, model } from 'mongoose';

import { Logger }                  from '@core';
import { IUser }                   from '@gql/interfaces';
import { getCurrentISOStringDate } from '@services/DateFromatter';
import { encrypt, compare }        from '@services/Encrypter';
import { encodeJWT }               from '@services/JWTManager';
import { IToken }                  from '@gql/interfaces/IToken';

const log: Logger = new Logger('database:UserRepository');

export class UserRepository {
  private name: string = 'User';
  private readonly model: Model<any>;

  constructor() {
    this.model = this.initModel();
  }

  public async create(args) {
    const newUser = this.setData(args);

    const result = await newUser.save();
    log.write(`User with id: ${result._id} was successfully saved`);
    return result;
  }

  public async getByField(field, value) {
    try {
      return await this.model.findOne({[field]: value});
    } catch (e) {
      throw new Error(e);
    }
  }

  public async authorize(args: any): Promise<IToken> | undefined {
    try {
      const user: IUser = await this.model.findOne({email: args.email});
      const checkPass = await compare(args.password, user.password);

      if (checkPass) {
        const token = encodeJWT({
          name: user.name,
          email: user.email,
          roles: user.roles
        });

        return {token};
      } else {
        return undefined;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  private initModel(): Model<any> {
    return model(
      this.name,
      new Schema({
        name     : String,
        email    : {type: String, required: true},
        password : {type: String, required: true},
        roles    : {type: Array, required: true, default: ['user']},
        createdAt: {type: Date, required: true},
        updatedAt: {type: Date, required: true}
      }).set('toJSON', {getters: true})
    );
  }

  private setData(args: any, isUpdate: boolean = false): any {
    const userData: IUser         = args;
    const currentDateTime: string = getCurrentISOStringDate();

    if (userData.password) {
      userData.password = encrypt(userData.password);
    }

    if (!isUpdate) {
      userData.createdAt = userData.updatedAt = currentDateTime;
      return new this.model(userData);
    }

    userData.updatedAt = currentDateTime;
    return userData;
  }
}

export const userRepository = new UserRepository();
