import { Schema, Model, model } from 'mongoose';

import { Logger }          from '@core';
import { Helper }          from '@services/Helper';
import { IQuestionSearch } from '@gql/interfaces';
import { IQuestion } from '@gql/interfaces';

const log: Logger = new Logger('database:QuestionsRepository');
const MAX_LIMIT = 50;
const QUERY_SEPARATOR = '||';

class QuestionsRepository {
  private name: string = 'Questions';
  private readonly model: Model<any>;

  constructor() {
    this.model = this.initModel();
  }

  public async getQuestions(params: IQuestionSearch) {
    try {
      const search = {};

      if (params.query) {
        params.query.split(params.querySeparator ? params.querySeparator : QUERY_SEPARATOR)
          .forEach((item) =>
            Object.assign(search, this.compileSearchString(item.split('='))));
      }

      log.write(`Searching for: ${JSON.stringify(search)}`);

      return await this.model
        .find(search)
        .cache(3600)
        .limit(params.limit ? params.limit : MAX_LIMIT);
    } catch (e) {
      throw new Error(e);
    }
  }

  public async addQuestion(args: IQuestion) {
    try {
      // implement set Answer to existing question
    } catch (e) {
      log.write(`Failed to set answer`);
      throw new Error(e);
    }
  }

  private compileSearchString(keyVal: Array<string>): Object | undefined {
    if (!keyVal[0] || !keyVal[1]) {
      return undefined;
    }

    let key               = keyVal[0];
    const expression: any = {};

    switch (key) {
      case 'ids':
        expression.$in = keyVal[1].split(',')
          .map((id) => parseInt(id, 10));
        break;
      case 'stem':
      case 'instructions':
        expression.$regex = `.*${keyVal[1]}.*`;
        break;
      case 'answers':
        key += '.value';
        expression.$regex = `.*${Helper.screenInvalidCharacters(keyVal[1])}.*`;
        break;
      default:
        return undefined;
    }
    return {[key]: expression};
  }

  private initModel(): Model<any> {
    return model(
      this.name,
      new Schema({
        ids          : {type: Array, required: true},
        instructions : {type: String, default: undefined},
        stem         : {type: String, required: true, default: undefined},
        image        : {type: String, default: undefined},
        answers      : {type: Array, required: true, default: []},
        correctAnswer: {type: String}
      }).set('toJSON', {getters: true})
    );
  }

  private setData(args: IQuestion): Document {
    return new this.model(args);
  }

}

export const questionsRepository = new QuestionsRepository();
