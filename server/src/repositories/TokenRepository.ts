import { Schema, Model, model, Document } from 'mongoose';

import { Logger }    from '@core';
import { IToken }    from '@gql/interfaces/IToken';
import { appConfig } from 'appConfig';

const log: Logger = new Logger('database:TokenRepository');

class TokenRepository {
  private name: string = 'Tokens';
  private readonly model: Model<any>;

  constructor() {
    this.model = this.initModel();
  }

  public async getToken(token: string) {
    try {
      return await this.model.findOne({token});
    } catch (e) {
      throw new Error(e);
    }
  }

  public async setToken(token: string): Promise<Document> {
    try {
      const tokenInDB = await this.getToken(token);
      if (tokenInDB) {
        return tokenInDB;
      }

      const newToken = this.setData({token});
      const result = await newToken.save();
      log.write(`Token with id: ${result._id} was successfully saved`);
      return result;
    } catch (e) {
      log.write(`Failed to save token: ${token}`);
      throw new Error(e);
    }

  }

  public async blacklistToken(token: string) {
    try {
      const tokenInDB = await this.getToken(token);
      if (tokenInDB) {
        tokenInDB.isBlacklisted = true;
        tokenInDB.save();
        return {
          success: true
        };
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  private initModel(): Model<any> {
    return model(
      this.name,
      new Schema({
        token    : {type: String, required: true},
        isBlacklisted: {type: Boolean, required: true, default: false},
        createdAt: { type: Date, default: Date.now, expires: appConfig.jwtExpire || '24h' }
      }).set('toJSON', {getters: true})
    );
  }

  private setData(args: IToken): Document {
    return new this.model(args);
  }

}

export const tokenRepository = new TokenRepository();
