import { Router } from 'express';

import { appConfig }    from 'appConfig';
import { PublicRoute }  from './public';
import { GraphqlRoute } from './graphql';

export class IndexRoutes  {
  public readonly endpoint: string = '/';
  public routes: Router            = Router();
  public setErrorRoutes: Function = PublicRoute.setErrorRoutes;

  constructor() {
    this.setPublicRoutes();
    this.setGraphQLRoute();
  }

  /**
   *  Set routes that will be showed to user
   */
  private setPublicRoutes() {
    const publicRoute: PublicRoute = new PublicRoute();
    this.routes.use(appConfig.endpoint.client, publicRoute.routes);
  }

  /**
   *  Set GraphQL main route
   */
  private setGraphQLRoute() {
    const graphqlRoute: GraphqlRoute = new GraphqlRoute();
    this.routes.use(appConfig.endpoint.api, graphqlRoute.routes);
  }
}
