import * as ExpressGraphQL from 'express-graphql';

import { Exception }                 from '@exceptions';
import { Schema }                    from '../schemas';
import { Router, Request, Response } from 'express';
import { Context, Empty }            from '@core';

export class GraphqlRoute {
  public routes: Router             = Router();
  private readonly endpoint: string = '/graphql';

  constructor() {
    this.setGraphQLSchema();
  }

  private setGraphQLSchema() {
    this.routes.use(this.endpoint, ExpressGraphQL((req: Request, res: Response) => ({
        schema     : Schema.get(),
        rootValue  : Empty,
        graphiql   : true,
        context    : new Context(req, res),
        formatError: (exception) => ({
          name   : Exception.getName(exception.message),
          message: Exception.getMessage(exception.message),
          path   : exception.path
        })
      })
      )
    );
  }
}
