import { Application, Request, Response, Router } from 'express';

import { appConfig } from 'appConfig';
import * as path     from 'path';

/**
 *  Front End client side & In case of other URL requests provide default route
 */
export class PublicRoute {
  public routes: Router             = Router();
  private readonly endpoint: string = '/';

  constructor() {
    this.setPublicRoute();
  }

  /**
   * Set main public route
   */
  private setPublicRoute() {
    this.routes.get(this.endpoint, (req: Request, res: Response) => {
      PublicRoute.sendIndexFile(res);
    });
  }

  /**
   * Set error routes 404 (will be handle by client)
   */
  static setErrorRoutes(app: Application) {
    app.use((req: Request, res: Response) => {
      PublicRoute.sendIndexFile(res);
    });
  }

  /**
   * File that will be send to client
   */
  static sendIndexFile(res: Response) {
    res.sendFile(appConfig.clientIndexFile, {
      root: path.join(process.cwd(),
        appConfig.folder.client)
    });
  }
}
