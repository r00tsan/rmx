const root = __dirname.replace('\config', '');

const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const NodemonPlugin = require( 'nodemon-webpack-plugin' );

module.exports = {
  entry        : `${root}/bin/www.ts`,
  target       : 'node',
  mode         : 'development',
  externals    : [
    /^[a-z\-0-9]+$/
  ],
  output       : {
    filename     : 'rmx-server.js',
    path         : `${root}/build`,
    libraryTarget: 'commonjs'
  },
  plugins: [
    new NodemonPlugin()
  ],
  resolve      : {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
    modules   : [
      `${root}/node_modules`
    ],
    plugins   : [
      new TsconfigPathsPlugin({configFile: `${root}/tsconfig.json`})
    ]
  },
  module       : {
    rules: [{
      test: /\.tsx?$/,
      use : 'ts-loader'
    }]
  }
};