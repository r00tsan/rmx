declare module 'mongoose' {
   interface DocumentQuery<T, DocType extends Document, QueryHelpers = {}> {
    cache(time: number): this;
  }
}
