declare namespace config {

  interface Environments {
    development: Configuration;
    test: Configuration;
    production: Configuration;
  }
  export interface Configuration {
    clientIndexFile: string;
    folder: WorkingFolders;
    endpoint: Endpoints;
    defaultPort: number;
    mongoUri: string;
    debug: string;
    jwtSecret: string;
    jwtExpire: string;
  }

  interface WorkingFolders {
    client: string;
    server: string;
  }

  interface Endpoints {
    client: string;
    api: string;
  }

}