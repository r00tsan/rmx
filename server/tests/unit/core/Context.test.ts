import * as httpMocks from 'node-mocks-http';

import { Context } from '@core';

describe('Context:', () => {
  let req = httpMocks.createRequest();
  let res = httpMocks.createResponse();

  test('Arguments and functions should be defined', () => {
    let context: Context<any> = new Context(req, res);

    context.setResolveArgument({test: 'it'});
    expect(context).toBeDefined();
    expect(context.Args).toBeDefined();
    expect(context.Request).toBeDefined();
    expect(context.Response).toBeDefined();
    expect(context.validateUser).toBeDefined();
    expect(context.hasUserRoles).toBeDefined();
  });

  test('validateUser', () => {
    req.user                  = {roles: ['testRole']};
    let context: Context<any> = new Context(req, res);

    const allowed      = context.validateUser(['testRole']),
          allowedEmpty = context.validateUser([]),
          notAllowed   = context.validateUser(['noRole']);

    expect(allowed.success && allowedEmpty.success).toBeTruthy();
    expect(notAllowed.success).toBeFalsy();
  });

  test('validateUnauthorizedUser', () => {
    req.user                  = {error: 'error'};
    let context: Context<any> = new Context(req, res);

    const notAllowed = context.validateUser(['testRole']);

    expect(notAllowed.success).toBeFalsy();
  });

});