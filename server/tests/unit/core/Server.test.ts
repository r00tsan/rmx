import * as supertest from 'supertest';
import * as mongoose  from 'mongoose';

import { Server }    from '@core';
import { appConfig } from 'appConfig';

const request = supertest(`http://localhost:${appConfig.defaultPort}`);

describe('Server Run Test', () => {

  beforeAll(async () => {
    await Server.start();
  });

  afterAll(async () => {
    await Server.running.close();
    await mongoose.disconnect();
  });

  test('Server port should be defined', () => expect(Server.port).toBe(appConfig.defaultPort));

  test('Server should be running', () => {
    request
      .get('/')
      .expect(200);
  });
});